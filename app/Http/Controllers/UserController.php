<?php

namespace App\Http\Controllers;

use ray;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserCreateRequest;

class UserController extends Controller
{
    public function createUser(UserCreateRequest $request)
    {
        sleep(3);
        $user = User::create($request->validated());
        $credentials = [
            'email' => $user->email,
            'password' => $request->password
        ];
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return back()->with('message', 'Inicio de sesión correcto');
        }
        return back()->withErrors([
            'message' => 'Lo sentimos, las credenciales ingresadas no coinciden',
        ]);
    }
    public function LogIn(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return back()->with('success', 'Inicio de sesión correcto');
        }
        return back()->withErrors([
            'message' => 'Lo sentimos, las credenciales ingresadas no coinciden',
        ]);
    }
}
